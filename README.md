# radio frequencies

## 70cm Band
[PMR](CSV/pmr.csv)

[BOS](CSV/BOS_70cm.csv)

## 2m Band
[Hamradio](CSV/hamradio_2m.csv)

[Freenet](CSV/freenet.csv)

[BOS](CSV/BOS_2m.csv)

## 4m Band
[BOS](CSV/BOS_4m.csv)

## 6m Band
[Hamradio 6m](CSV/hamradio_6m.csv)

## 10m Band
[CB](CSV/citizens-band.csv)

## Hamradio KW
[Hamradio](CSV/hamradio_kw.csv)

